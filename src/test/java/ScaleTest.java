import fruit.Fruit;
import fruit.Melon;
import fruit.NewScale;
import org.junit.Test;

import static org.junit.Assert.*;

public class ScaleTest {

    @Test
    public void init() {
        NewScale newScale = new Scale();

        newScale.init();

        Fruit melon = new Melon(2,21);

        newScale.controlFruit(melon);

        newScale.init();

        double totalSum = newScale.totalSum();

        assertEquals(0, totalSum, 0.0f);
    }

    @Test
    public void controlFruit() {
        NewScale newScale = new Scale();

        newScale.init();

        Fruit melon = new Melon(2,21);

        newScale.controlFruit(melon);

        double totalSum = newScale.totalSum();

        assertEquals(42, totalSum, 0.0f);
    }

    @Test
    public void totalSum() {
    }
}