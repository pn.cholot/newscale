package fruit;

public abstract class Fruit {
    protected final double price;

    public Fruit(double price) {
        this.price = price;
    }

    public abstract double computeScale();

}

