package fruit;

public class FruitByWeight extends Fruit {
    protected final double weight;

    public double getWeight() {
        return weight;
    }

    public FruitByWeight(double price, double weight) {
        super(price);
        this.weight = weight;

    }

    @Override
    public double computeScale(){
        return price*weight;
    }

}
