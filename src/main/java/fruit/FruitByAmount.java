package fruit;

public class FruitByAmount extends Fruit{
    protected double amount;

    public double getAmount() {
        return amount;
    }

    public FruitByAmount(double price, double amount) {
        super(price);
        this.amount = amount;
    }

    @Override
    public double computeScale(){
        return price*amount;
    }
}
