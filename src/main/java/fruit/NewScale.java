package fruit;

public interface NewScale {

    void init();

    void controlFruit(Fruit fruit);

    double totalSum();

}
