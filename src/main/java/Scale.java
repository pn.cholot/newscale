import fruit.Fruit;
import fruit.NewScale;

public class Scale implements NewScale {

    private double totalSum;

    public void init() {
        totalSum = 0;
    }

    public void controlFruit(Fruit fruit) {
        totalSum += fruit.computeScale();
    }

    public double totalSum() {
        return totalSum;
    }
}
